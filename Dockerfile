FROM debian:bullseye-20231009

RUN dpkg --add-architecture i386
RUN apt-get update

# install curl and gpg
RUN apt-get -y --no-install-recommends install \
    ca-certificates \
    gnupg \
    curl \
    sudo \
    procps \
    wget \
    netbase \
    dirmngr \
    bash-completion \
    libc-dbg:i386 \
    iputils-ping \
    dumb-init && \
    apt-get -q -y autoremove && \
    rm -rf /var/lib/apt/lists/* /var/tmp/* /tmp/*

COPY resources/SoFa_09C2ED61_Public.asc .
RUN cat SoFa_09C2ED61_Public.asc | apt-key add - && \
    echo "deb https://nexus.rd.softathome.com/repository/apt sah main" >> \
    /etc/apt/sources.list.d/softathome.list && \
    apt update && \
    rm SoFa_09C2ED61_Public.asc

# set shell to bash
RUN echo "dash dash/sh boolean false" | debconf-set-selections
RUN DEBIAN_FRONTEND=noninteractive dpkg-reconfigure dash
RUN ln -s /bin/mkdir /usr/bin/mkdir

ENV TERM xterm-256color
ENV LANG=C.UTF-8

# Set the locale to en_US.UTF-8
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y locales

RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LC_COLLATE="en_US.UTF-8"

COPY resources/ /usr/local/bin/
COPY resources/sudoers /etc/sudoers
RUN chmod 644 /etc/sudoers

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["/usr/local/bin/init.sh"]
