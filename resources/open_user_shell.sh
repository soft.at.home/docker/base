#!/bin/bash

if [[ $# -eq 0 ]]; then
    echo "Open shell"
    if [ -z "$USER" ]; then
        exec /bin/bash
    else
    	cd /home/"$USER" || exit
        if [ ! -e /home/"$USER"/.ssh/id_rsa ] && [ -d /root/.ssh ]; then
            mkdir -p /home/"$USER"/.ssh
            cp /root/.ssh/* /home/"$USER"/.ssh/
            chown -R "$USER":normal_users /home/"$USER"/.ssh/
        fi
        exec su "$USER"
    fi
else
    if [ -z "$USER" ]; then
        exec "$*"
    else
    	cd /home/"$USER" || exit
        exec su -c "$*" "$USER"
    fi
fi
