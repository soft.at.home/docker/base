#!/usr/bin/env bash
if [ $# -eq 0 ] || [ ! -f "${1}" ]; then
    echo -e "\tError: no key given"
    exit 1
fi

mkdir -p ~/.ssh && chmod 700 ~/.ssh
cp "${1}" ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
