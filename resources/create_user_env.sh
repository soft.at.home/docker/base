#!/bin/bash
if [ -n "$USER" ]; then
    USER_UID=$UID
    if [ -z "$USER_UID" ]; then
        USER_UID=1000
    fi
    if [ $USER_UID == 0 ]; then
        USER_UID=1000
    fi
    if [ -z "$GID" ]; then
        GID=1000
    fi

    echo "Add group $GID"
    addgroup --gid $GID "normal_users"
    echo "Add user $USER_UID in group $GID"
    useradd -m -u $USER_UID -g $GID -s /bin/bash "$USER"  > /dev/null 2>&1
    usermod -a -G sudo "$USER"
    echo "Set owner of /home/$USER"
    chown $USER_UID:$GID /home/"$USER"
    echo "Set user files"
    if [ ! -x /home/"$USER"/.bashrc ]; then
        cp /etc/skel/.bashrc /home/"$USER"
        chown $USER_UID:$GID /home/"$USER"/.bashrc
    fi
    if [ ! -x /home/"$USER"/.profile ]; then
        cp /etc/skel/.profile /home/"$USER"
        chown $USER_UID:$GID /home/"$USER"/.profile
    fi
    if [ ! -x /home/"$USER"/.bash_logout ]; then
        cp /etc/skel/.bash_logout /home/"$USER"
        chown $USER_UID:$GID /home/"$USER"/.bash_logout
    fi

    if [ -n "$OWNS" ]; then
        for dir in $OWNS; do
            echo "Set owner of $dir"
            chown -R $USER_UID:$GID "$dir"
        done
    fi

    if [ -f "$SSH_PRIVATE_KEY" ]; then
        su -c "seed_private_key.sh $SSH_PRIVATE_KEY" "$USER"
    fi
    echo "User configuration done"
fi
