#!/bin/bash

echo "Starting ..."

if [ -z "${USER}" ]; then
    echo "No user provided - setting user = [sahbot]"
    export USER="sahbot"
fi

if [ ! -f /var/run/dinit ]; then
    create_user_env.sh
    touch /var/run/dinit
fi

open_user_shell.sh

exit $?
