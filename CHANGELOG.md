# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.2.1 - 2024-09-16(09:37:14 +0000)

### Other

- ci: add IMAGE_PATH variable to correctly push opensource container

## Release v1.2.0 - 2024-04-03(15:46:56 +0000)

### Other

- - [WIFI7] First integration of QSDK ATH 12.4

## Release v1.1.0 - 2023-10-19(13:00:35 +0000)

### Security

- Update to bullseye-20231009

## Release v1.0.9 - 2023-08-16(09:23:08 +0000)

### Fixes

- Remove perforce sources

## Release v1.0.8 - 2023-07-04(13:16:04 +0000)

## Release v1.0.7 - 2022-08-24(13:21:41 +0000)

### Fixes

- Open user shell script checks for existence of id_rsa on wrong place

